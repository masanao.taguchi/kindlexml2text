@echo off
set XSLTPROC=c:\oss\libxslt\bin\xsltproc.exe
set DEST=m:\.kindle
set OUTPUT=%DEST%\kindle.txt
set INPUT=%LOCALAPPDATA%\Amazon\Kindle\Cache\KindleSyncMetadataCache.xml

if NOT EXIST %INPUT% (
	ECHO 入力ファイルが存在しない
	GOTO END
)

if EXIST %OUTPUT% (
	FOR %%a IN (%INPUT%)  DO SET TIMESTAMP1=%%~ta
	FOR %%b IN (%OUTPUT%) DO (
		SET TIMESTAMP2=%%~tb
	)

	IF "%TIMESTAMP2%" GTR "%TIMESTAMP1%" (
		ECHO 入力のほうが古いため処理しない
		GOTO END
	)
)

%XSLTPROC%  %DEST%\toText.xslt %INPUT% > %OUTPUT%

:END
@echo on