<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="text" encoding="UTF-8"/>
   <xsl:template match="/">"ASIN","Authors","Title","Publishers","Publication_Date","Purchase_Date"
<xsl:apply-templates select="response/add_update_list"/></xsl:template>

   <xsl:template match="meta_data"><xsl:if test="not(title = '---------------')">
	<xsl:variable name="concatenated"><xsl:apply-templates select="authors"/></xsl:variable><!--
	-->"<xsl:value-of select="ASIN"/>,<!--
	-->"<xsl:copy-of
	   select="substring($concatenated, 0, string-length($concatenated))"/>",<!--
        -->"<xsl:value-of select="title"/>",<!--
	-->"<xsl:value-of select="publishers"/>",<!--
        -->"<xsl:value-of select="publication_date"/>",<!--
        -->"<xsl:value-of select="purchase_date"/>"	
<!--
	--><xsl:text disable-output-escaping="yes"></xsl:text>
     </xsl:if></xsl:template>

   <xsl:template match="author">
     <xsl:value-of select="."/>×</xsl:template>
</xsl:stylesheet>
