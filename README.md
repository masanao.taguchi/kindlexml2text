# 目的
Kindle for PCの内容をテキストファイルに変換する

# 稼働環境

- toText.xslt

    xsltprocが必要。
    
    他のXSLTプロセッサでも動くと思いますが、結果ツリーノードセットを利用しているため、XSLT ver.2以上が必要なはずです(msxslでは動かないはず)。

    各環境での導入方法は利用方法のところで解説します。

# 利用方法

## Windows or Mac のみ

xsltprocの引数に```toText.xslt```と ```KindleSyncMetadataCache.xml``` を指定し、テキストファイルへリダイレクトします。
```shell
mtag@mtag:.kindle $ xsltproc toText.xslt KindleSyncMetadataCache.xml > out.txt
```

- xsltproc

    + Mac

        /usr/bin/xsltprocに標準で入っているので何もしなくても使えます

    + Windows(wslあり)の場合

        [linuxbrew](https://docs.brew.sh/Homebrew-on-Linux) を導入し、xsltprocを入れることをお勧めします。

        yum/dnf/aptなどでも入るはずです。

    + Windows(wslなし)の場合

        libxsltについているxsltprocをインストールします。

        1. https://saj.co.jp/techinfo/htmls/xml-libxslt-install_windows_precompile.html の手順に従いダウンロードします。

        1. libxslt-x.x.xx.win32.zip(x.x.xxはバージョン番号)を展開します

- updateKindleText (Windowsの場合、wslがないと動きません)

	+ Mac/Windows+WSLの環境で、kindleの生成したxmlから直接csvに変換します
    
    + スクリプトと同じフォルダに出力されます
	
	+ Windowsの場合、ユーザーのLocalAppDataフォルダを特定する必要があります。
		
		* $LOCALAPPDATAが取得できない場合,環境変数を内部で取得するので特に何も設定せずにファイルを取得することができます(wslのユーザーとkindleのユーザーが同じ場合)
		
		* [WSLENV](https://docs.microsoft.com/ja-jp/windows/wsl/filesystems#share-environment-variables-between-windows-and-wsl-with-wslenv)経由で、取得することも可能
        
            (若干早いはず。が、あまり意味はない)

## Windows(wslなし) + Linux

Windows上のファイルをLinuxに持っていき処理する場合用にcopycache.batを用意しています。
私は、copycache.bat をWindowsのタスクスケジューラ仕込んでいます。

- copycache.bat

    Windows上で実行するためのバッチファイルです。
    ファイルの置き場所のPATHが先頭にあるので、利用する環境にあわせて修正します。
	
	Windows上で変換せず、linux環境にコピーして変換する場合に利用します

ファイルの生成・コピーがWindows側で、変換がlinux側になるので、スケジューラでの運用にはタイムスタンプなどを利用する必要があります

## linuxのみ

kindle for linuxが提供されていないので、ひとまず除外します。

Wineなどを使えば動くのかもしれません。

## 
