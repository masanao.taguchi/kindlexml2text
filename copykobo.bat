@echo off
set DEST=m:\.kindle\
set NAME=Kobo.sqlite
set OUTPUT=%DEST%\%NAME%
set INPUT="%LOCALAPPDATA%\Kobo\Kobo Desktop Edition\%NAME%"

if NOT EXIST %INPUT% (
	ECHO 入力ファイルが存在しない
	GOTO END
)

if EXIST %OUTPUT% (
	FOR %%a IN (%INPUT%)  DO SET TIMESTAMP1=%%~ta
	FOR %%b IN (%OUTPUT%) DO SET TIMESTAMP2=%%~tb
	IF "%TIMESTAMP2%" GEQ "%TIMESTAMP1%" (
		GOTO END
	)
)

COPY %INPUT% %OUTPUT%

:END
@echo on